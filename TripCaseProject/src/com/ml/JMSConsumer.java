package com.ml;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.Queue;
import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ml.utility.TripCaseUtility;

public class JMSConsumer extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Resource(name = "foo")
    private Topic fooTopic;

    @Resource(name = "bar")
    private Queue barQueue;

    @Resource
    private ConnectionFactory connectionFactory;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //...
    	try{
    		TripCaseUtility tripUtility = new TripCaseUtility();
    		
    		Properties properties = new Properties(); 
    		properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, 
    		"org.apache.activemq.jndi.ActiveMQInitialContextFactory"); 
    		properties.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616"); 
    		        
    		InitialContext context = new InitialContext(properties); 
    		ConnectionFactory cf = (ConnectionFactory) context.lookup("ConnectionFactory"); 
    		
    		
            Connection connection = cf.createConnection();
            connection.start();
            
            System.out.println("Connection Object From Consumer..." + connection.getClientID().toString());
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer consumer = session.createConsumer(barQueue);
            
            Message message =  consumer.receive();
            String body = ( (TextMessage)message).getText();
            System.out.println("Consumed Message Before parsing..." + body.toString());
            tripUtility.Annotate(body);
           
            System.out.println("Consumed Messagetext afetr parsing");

            connection.close();
            
        
    	}catch(JMSException je){
    		je.printStackTrace();
    	}catch(NamingException je){
    		je.printStackTrace();
    	}
        //...
    	
    	
    }

    private static String env(String key, String defaultValue) {
        String rc = System.getenv(key);
        if( rc== null )
            return defaultValue;
        return rc;
    }

    private static String arg(String []args, int index, String defaultValue) {
        if( index < args.length )
            return args[index];
        else
            return defaultValue;
    }
}