package com.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.activemq.command.ActiveMQQueue;

import com.ml.utility.TripCaseUtility;
import com.ml.vo.TravellerInfoVO;

public class JMSConsumerOne extends HttpServlet {

	private static final long serialVersionUID = -2710077995046145848L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	Connection connection = null;
    	try{
    		TripCaseUtility tripUtility = new TripCaseUtility();
    		TravellerInfoVO infoVO = null;
    		List<TravellerInfoVO> infoVOList = new ArrayList<TravellerInfoVO>();
    		
    		Properties properties = new Properties(); 
    		properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, 
    		"org.apache.activemq.jndi.ActiveMQInitialContextFactory"); 
    		properties.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");     		        
    		InitialContext context = new InitialContext(properties); 
    		ConnectionFactory cf = (ConnectionFactory) context.lookup("ConnectionFactory");     		
    		
            connection = cf.createConnection();
            connection.start();
            
            //TextListener listener = new TextListener();
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);      
            MessageConsumer consumer = session.createConsumer(new ActiveMQQueue("testqueue"));
            
         // create a queue browser
            QueueBrowser queueBrowser = session.createBrowser(new ActiveMQQueue("testqueue"));
            Enumeration e = queueBrowser.getEnumeration();            
            int numMsgs = 0;
            
            while(e.hasMoreElements()) {
	            Message message =  consumer.receive();
	            if (message instanceof ObjectMessage){
	            	infoVO = new TravellerInfoVO();
	            	Object obj = ((ObjectMessage)message).getObject();
	            	infoVO = (TravellerInfoVO) obj; 
	            	
	            	infoVOList.add(infoVO);
	            }
	            numMsgs++;
	            System.out.println("testqueue has:: " + numMsgs + " messages");  
	           // tripUtility.Annotate(infoVO.getEmailInput().toString());  //generate the xml response	
	                   	
		            	sendResponseData(connection,infoVO);     
		           
		            	receiveResponseData(connection);
	            
            }
            /* below code for resending parsed message back to producer */
           // System.out.println("infoVOList.size() :: " + infoVOList.size()); 
            /*if(infoVOList.size() > 0){
	            for(int i=0;i<10;i++){	            	
	            	sendResponseData(connection,i);     
	            }
            }else{
            	System.out.println("infoVOList.size() is empty "); 
            }*/
            
            /*consuming the response from server after parsing the email data */
           // receiveResponseData(connection);
        
    	}catch(JMSException je){
    		je.printStackTrace();
    	}catch(NamingException ne){
    		ne.printStackTrace();
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		if(connection != null){
    			try{
    				connection.close();
    			}catch(Exception e){
    				e.printStackTrace();
    			}
    		}
    	    	
    	}
    }
    
    //method to send parsed data to the client
    public void sendResponseData(Connection connection,TravellerInfoVO infoVO) throws Exception{

    	Session sessionReturn = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 
        MessageProducer producerReturn = sessionReturn.createProducer(new ActiveMQQueue("testqueuereturn"));
        producerReturn.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        //producerReturn.send(sessionReturn.createObjectMessage((new TravelInfoMock()))); //where TravelInfoMock is a mock data for travelinfo.
        producerReturn.send(sessionReturn.createObjectMessage((infoVO.getEmailInput())));
    }
    
    //method to consume the parsed data(at client side)
    public void receiveResponseData(Connection connection)throws Exception{
    	Session sessionConsumer = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);      
        MessageConsumer consumeConsumer = sessionConsumer.createConsumer(new ActiveMQQueue("testqueuereturn"));
        
        TravellerInfoVO infoVO1 = new TravellerInfoVO();
        String msg = null;
        
     // create a queue browser
        QueueBrowser queueBrowser = sessionConsumer.createBrowser(new ActiveMQQueue("testqueuereturn"));
        Enumeration e = queueBrowser.getEnumeration();            
        int numMsgs = 0;        
     //   while(e.hasMoreElements()){
	        Message message =  consumeConsumer.receive();
	        if (message instanceof ObjectMessage){	        	
	        	Object obj = ((ObjectMessage)message).getObject();
	        	msg = (String) obj;
	        }
	        numMsgs++;
            System.out.println("testqueuereturn has:: " + numMsgs + " messages");  
       // }
        System.out.println("Received Message after parsing at Client side..." + msg);    
    }
}