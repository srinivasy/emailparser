package com.ml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.Queue;
import javax.jms.ConnectionFactory;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.Service;
import org.apache.activemq.broker.region.Region;
import org.apache.activemq.broker.region.RegionBroker;

import com.ml.emailreader.EmailReader;
import com.ml.utility.TripCaseUtility;


import javax.mail.MessagingException;

import javax.mail.internet.MimeMessage;

import com.ml.emailreader.EmailReader;
import com.ml.utility.TripCaseUtility;



public class JMSProducer extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Resource(name = "foo")
    private Topic fooTopic;

    @Resource(name = "bar")
    private Queue barQueue;

    @Resource
    private ConnectionFactory connectionFactory;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //...
    	try{
    		
    		Properties properties = new Properties(); 
    		properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, 
    		"org.apache.activemq.jndi.ActiveMQInitialContextFactory"); 
    		properties.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616"); 
    		        
    		InitialContext context = new InitialContext(properties); 
    		ConnectionFactory cf = (ConnectionFactory) context.lookup("ConnectionFactory"); 
    		
    		
            Connection connection = cf.createConnection();
            connection.start();
            
            System.out.println("Connection Object From Producer..." + connection.getClientID().toString());
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(barQueue);
            
            getEmialString(producer,session);
            
           // producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
          //  producer.send(session.createTextMessage("Hello Message From JMSProducer"));
            
            
            
            System.out.println("Message Produced and ready for consumer");
            connection.close();
            
        
    	}catch(JMSException je){
    		je.printStackTrace();
    	}catch(NamingException je){
    		je.printStackTrace();
    	}
        //...
    	
    	
    }

    private static String env(String key, String defaultValue) {
        String rc = System.getenv(key);
        if( rc== null )
            return defaultValue;
        return rc;
    }

    private static String arg(String []args, int index, String defaultValue) {
        if( index < args.length )
            return args[index];
        else
            return defaultValue;
    }
    
    public void getEmialString( MessageProducer producer,javax.jms.Session JMSSsession){
    	
    	Properties p = System.getProperties();
    	javax.mail.Session Mailsession = javax.mail.Session.getInstance(p);
		MimeMessage mimeMessage;
		String emailString = "";
		
		File inputDir = null;
		try {
			inputDir = new File("C:\\reports\\tmpinput\\");
			File[] files = inputDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				System.out.println("Input File size indicator ::::" + i);
	            if (!files[i].isDirectory()) {
	            	
	            	mimeMessage = new MimeMessage(Mailsession, new FileInputStream(files[i]));
	    			emailString = EmailReader.convertToString(mimeMessage);
	    			
	    			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
	    	        producer.send(JMSSsession.createTextMessage(emailString));
	    			
	             
	            }
	          }
		}catch(Exception e){
			e.printStackTrace();
		}
		
    }

}