package com.ml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.Queue;
import javax.jms.ConnectionFactory;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.Service;
import org.apache.activemq.broker.region.Region;
import org.apache.activemq.broker.region.RegionBroker;
import org.apache.activemq.command.ActiveMQQueue;

import com.ml.emailreader.EmailReader;
import com.ml.utility.TripCaseUtility;


import javax.mail.MessagingException;

import javax.mail.internet.MimeMessage;

import com.ml.emailreader.EmailReader;
import com.ml.utility.TripCaseUtility;
import com.ml.vo.TravellerInfoVO;

import javax.jms.JMSContext;

public class JMSProducerTwo extends HttpServlet {

	
	private static final long serialVersionUID = -3613975886620037462L;


	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	 Connection connection = null;
    	try{
    		
    		
    		
    		Properties properties = new Properties(); 
    		properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, 
    		"org.apache.activemq.jndi.ActiveMQInitialContextFactory"); 
    		properties.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616"); 
    		        
    		InitialContext context = new InitialContext(properties); 
    		ConnectionFactory cf = (ConnectionFactory) context.lookup("ConnectionFactory");     		
            connection = cf.createConnection();
            connection.start();
            
            
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);                        
            MessageProducer producer = session.createProducer(new ActiveMQQueue("testqueue"));      
            
            
           // session.setMessageListener()
            getEmialString(producer,session);             
            System.out.println("Message Produced and ready for consumer");
            
    	}catch(JMSException je){
    		je.printStackTrace();
    	}catch(NamingException je){
    		je.printStackTrace();
    	}finally{
    		if(connection != null){
    			try{
    				connection.close();
    			}catch(Exception e){
    				e.printStackTrace();
    			}
    		}
    	}
    }

   
    public void getEmialString( MessageProducer producer,javax.jms.Session JMSSsession){
    	
    	Properties p = System.getProperties();
    	javax.mail.Session Mailsession = javax.mail.Session.getInstance(p);
		MimeMessage mimeMessage;
		String emailString = "";
		
		File inputDir = null;
		try {
			inputDir = new File("C:\\reports\\tmpinput\\");
			File[] files = inputDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				System.out.println("Input File size indicator ::::" + i);
	            if (!files[i].isDirectory()) {
	            	
	            	mimeMessage = new MimeMessage(Mailsession, new FileInputStream(files[i]));
	    			emailString = EmailReader.convertToString(mimeMessage);
	    			
	    			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
	    	        
	    	        
	    			//send email string through srializable java  object to consumer for parsing
	    	        TravellerInfoVO infoVO = new TravellerInfoVO();
	    	        infoVO.setEmailInput(emailString);
	    	        ObjectMessage objectMessage  = JMSSsession.createObjectMessage(infoVO);
	    	        
	    	        
	    	        producer.send(objectMessage);
	             
	            }
	          }
		}catch(Exception e){
			e.printStackTrace();
		}
		
    }

}