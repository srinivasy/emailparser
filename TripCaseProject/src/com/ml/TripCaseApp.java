package com.ml;

//import NLPCore;

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.semgraph.*;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.graph.*;
import edu.stanford.nlp.fsm.*;
import edu.stanford.nlp.classify.*;
import edu.stanford.nlp.sequences.*;
import edu.stanford.nlp.wordseg.*;
import edu.stanford.nlp.util.*;

import java.lang.*;

import edu.stanford.nlp.dcoref.*;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.process.*;

import java.nio.file.*;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import com.ml.emailreader.EmailReader;
import com.ml.utility.TripCaseUtility;

public class TripCaseApp
{

	//public StanfordCoreNLP pipeline;
	public void getParserDetails() {
		
		Properties p = System.getProperties();
		Session session = Session.getInstance(p);
		MimeMessage mimeMessage;
		String emailString = "";
		TripCaseUtility tripUtility = new TripCaseUtility();
		File inputDir = null;
		try {
			inputDir = new File("C:\\reports\\input\\");
			File[] files = inputDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				System.out.println("Parsing Success indicator ::::" + i);
	            if (!files[i].isDirectory()) {
	            	
	            	mimeMessage = new MimeMessage(session, new FileInputStream(files[i]));
	    			emailString = EmailReader.convertToString(mimeMessage);
	    			
	    			tripUtility.Annotate(emailString);
	             
	            }
	          }
			
			/*mimeMessage = new MimeMessage(session, new FileInputStream("C:\\reports\\input\\2019587.eml"));
			emailString = EmailReader.convertToString(mimeMessage);
			
			tripUtility.Annotate(emailString);*/
			
			// create an empty Annotation just with the given text
			//Annotation document = new Annotation(emailString);
			
			//tripUtility.getTokenDetails(document);
			System.out.println("Parsing Success ::::");
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	
	

}