package com.ml.emailreader;
//import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
//import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message.RecipientType; 
import javax.mail.MessagingException; 
import javax.mail.Multipart; 
import javax.mail.Session; 
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart; 
import javax.mail.internet.MimeMessage;

public class EmailReader {

//	public static void main(String[] args) throws IOException
//	{
//		try
//		{
//			Properties p = System.getProperties();
//			Session session = Session.getInstance(p);
//
//			MimeMessage mimeMessage = new MimeMessage(session, new FileInputStream("c:\\temp\\2019587.eml"));
//
//			System.out.println("Converted EMAIL IS ************** " +  convertToString(mimeMessage) );
//			// FileOutputStream os = new FileOutputStream(new File("c:\\message.txt"));
//			// mimeMessage.writeTo(os);
//
//		}
//		catch (Exception ex)
//		{
//			System.out.println(ex.getMessage());
//		}
//	}


	public static String convertToString(MimeMessage mimeMessage)
	{
		List<BodyPart> allBodyParts = new ArrayList<BodyPart>();
		String emailBody = "";
		try {
			InternetAddress[] toRecipients = (InternetAddress[]) mimeMessage.getRecipients(RecipientType.TO);
			InternetAddress[] ccRecipients = (InternetAddress[]) mimeMessage.getRecipients(RecipientType.CC);
			InternetAddress[] bccRecipients = (InternetAddress[]) mimeMessage.getRecipients(RecipientType.BCC);

			if (toRecipients != null)
			{
				String displayTo = "";

				for (int i = 0; i < toRecipients.length; i++)
				{
					System.out.println("Receipients Address : " + toRecipients[i].getAddress());

					if (toRecipients[i].getPersonal() != null)
					{
						displayTo += toRecipients[i].getPersonal();
					}
					else
					{
						displayTo += toRecipients[i].getAddress();
					}
				}
				System.out.println("All receipients " + displayTo);
			}

			if (mimeMessage.getFrom() != null && mimeMessage.getFrom().length > 0)
			{
				InternetAddress from = (InternetAddress) mimeMessage.getFrom()[0];

				if (from.getPersonal() != null)
				{
					System.out.println("From Personal : "+ from.getPersonal());
				}
				if (from.getAddress() != null)
				{
					System.out.println("From Address : " + from.getAddress());
				}

			}

			emailBody = EmailReader.getBodyAsString(mimeMessage);
		
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emailBody;

	}

	private static String getBodyAsString(MimeMessage mimeMessage)
	{
		String emailContent = "";
		List<BodyPart> allBodyParts = new ArrayList<BodyPart>();

		try {
			if (mimeMessage.getContent() instanceof Multipart)
			{
				getAllBodyParts((Multipart)mimeMessage.getContent(), allBodyParts);

				for (int i = 0; i < allBodyParts.size(); i++)
				{
					if (allBodyParts.get(i).isMimeType("text/plain") && allBodyParts.get(i).getDisposition() != null)
					{
						String content = (String) allBodyParts.get(i).getContent();
						byte[] buffer = content.getBytes("UTF-8"); //here you can use char set from allBodyParts.get(i)

					}
					else if (allBodyParts.get(i).isMimeType("text/plain"))
					{
						if(allBodyParts.get(i).getContent() instanceof MimeBodyPart)
						{
							MimeBodyPart mimeBodyPart = (MimeBodyPart)allBodyParts.get(i).getContent();
							System.out.println("getDescription********: " + mimeBodyPart.getDescription());
							emailContent = mimeBodyPart.getDescription();
						}
						else if (allBodyParts.get(i).getContent() instanceof String)
						{
							//System.out.println("getContent **************" + (String)allBodyParts.get(i).getContent());
							emailContent = (String)allBodyParts.get(i).getContent();
						}
					}
				}
			}
		}
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return emailContent;

	}

	private static void getAllBodyParts(Multipart multipart, List<BodyPart> allBodyParts) throws MessagingException, IOException
	{
		for (int i = 0; i < multipart.getCount(); i++)
		{
			BodyPart bodyPart = multipart.getBodyPart(i);
			allBodyParts.add(bodyPart);

			if(bodyPart.getContent() instanceof Multipart)
			{
				getAllBodyParts((Multipart)bodyPart.getContent(), allBodyParts);
			}
		}
	}
	
    //  TODO : To be enhanced in later stages //http://www.independentsoft.com/jmsg/tutorial/convertmimetomsg.html
	private static void getBody(MimeMessage mimeMessage)
	{
		List<BodyPart> allBodyParts = new ArrayList<BodyPart>();

		try {
			if (mimeMessage.getContent() instanceof Multipart)
			{
				getAllBodyParts((Multipart)mimeMessage.getContent(), allBodyParts);

				for (int i = 0; i < allBodyParts.size(); i++)
				{
					if (allBodyParts.get(i).isMimeType("text/plain") && allBodyParts.get(i).getDisposition() != null)
					{
						String content = (String) allBodyParts.get(i).getContent();
						byte[] buffer = content.getBytes("UTF-8"); //here you can use char set from allBodyParts.get(i)

					}
					else if (allBodyParts.get(i).isMimeType("text/plain"))
					{
						if(allBodyParts.get(i).getContent() instanceof MimeBodyPart)
						{
							MimeBodyPart mimeBodyPart = (MimeBodyPart)allBodyParts.get(i).getContent();
							System.out.println("getDescription********: " + mimeBodyPart.getDescription());
						}
						else if (allBodyParts.get(i).getContent() instanceof String)
						{
							System.out.println("getContent **************" + (String)allBodyParts.get(i).getContent());
						}
					}
					else if (allBodyParts.get(i).isMimeType("text/html"))
					{
						if(allBodyParts.get(i).getContent() instanceof MimeBodyPart)
						{
							MimeBodyPart mimeBodyPart = (MimeBodyPart)allBodyParts.get(i).getContent();

							String htmlText = mimeBodyPart.getDescription();

							System.out.println("htmlText****************** :" + htmlText);

						}
						else if (allBodyParts.get(i).getContent() instanceof String)
						{
							String htmlText = (String)allBodyParts.get(i).getContent();

							System.out.println("htmlText@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ :" + htmlText);
						}
					}
					else if (allBodyParts.get(i).getContent() instanceof InputStream)
					{
						InputStream is = (InputStream) allBodyParts.get(i).getContent();

						if(allBodyParts.get(i).getHeader("Content-Location") != null && allBodyParts.get(i).getHeader("Content-Location").length > 0)
						{
							String contentId = (String)allBodyParts.get(i).getHeader("Content-Location")[0];

						}
						else if(allBodyParts.get(i).getHeader("Content-ID") != null && allBodyParts.get(i).getHeader("Content-ID").length > 0)
						{
							String contentId = (String)allBodyParts.get(i).getHeader("Content-ID")[0];
							System.out.println("contentId&&&&&&&" + contentId);

						}

					}

				}
			}
			else if (mimeMessage.getContent() instanceof String)
			{
				if(mimeMessage.isMimeType("text/html"))
				{            
					String htmlText = (String)mimeMessage.getContent();
					System.out.println("htmlText%%%%%%%%%%%%%%%%%%%%%%%%%% :" + htmlText);

				}
				else   
				{ 
					String plainText = (String)mimeMessage.getContent();
					System.out.println("plainText^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ :" + plainText);
				}
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}


