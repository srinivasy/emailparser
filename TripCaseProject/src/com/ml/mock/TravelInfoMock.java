/**
 * 
 */
package com.ml.mock;

import java.io.Serializable;
import java.util.Date;

import org.junit.BeforeClass;

import com.ml.vo.AirInfoVO;
import com.ml.vo.TravellerInfoVO;
import static org.mockito.Mockito.*;

/**
 * @author Srinivas
 *
 */
public class TravelInfoMock implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static TravellerInfoVO trevlInfoVO;
	
	public static AirInfoVO airInfoVO;
	
	@BeforeClass
	public static void setUp(){
		trevlInfoVO =	mock(TravellerInfoVO.class);	
		
		airInfoVO = new AirInfoVO();
		airInfoVO.setEmailID("tom@gmail.com");
		airInfoVO.setAlternateFlightSchedule("27th March");
		airInfoVO.setDateOfDeparture(new Date());
		airInfoVO.setFlightNumber("A12747");
		airInfoVO.setSupplierPhoneNo(1234567);
		airInfoVO.setSuppplierCode("AT1212");
		
		when(trevlInfoVO.getAirEntityVO()).thenReturn(airInfoVO);
	}
	
	public AirInfoVO getAirInfoMockData(){
		return trevlInfoVO.getAirEntityVO();
	}
}
