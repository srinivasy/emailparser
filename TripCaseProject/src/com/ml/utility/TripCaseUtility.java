package com.ml.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;

public class TripCaseUtility {
	public StanfordCoreNLP pipeline;
	
	public void Annotate( String inputText)
	{

		System.out.println("Parsing consumed data in TripCaseUtility:Annotate() ");
		// creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution 
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		//props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref,sentiment");
		props.setProperty("sutime.binders", "0");

		//createModels( props);
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(inputText);

		// run all Annotators on this text
		pipeline.annotate(document);
		
		
		

		FileOutputStream os;
		try {
			
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy_HHmmss");
			
			String filename = "C:\\reports\\output\\" + "output_"+sdf.format(cal.getTime()) + ".xml";
			File xmloutput = new File(filename);
			xmloutput.createNewFile();
			os = new FileOutputStream(xmloutput);
			
			pipeline.xmlPrint(document, os);
			
			//String strOutput = pipeline.getEncoding();
			
			
			//pipeline.process(inputText);
			System.out.println("XML outpt generated");
			
			
			
			
			
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{

		}

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		for(CoreMap sentence: sentences) {
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				// this is the NER label of the token
				String ne = token.get(NamedEntityTagAnnotation.class);       
			}

			// this is the parse tree of the current sentence
			Tree tree = sentence.get(TreeAnnotation.class);

			// this is the Stanford dependency graph of the current sentence
			SemanticGraph dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		}

		// This is the coreference link graph
		// Each chain stores a set of mentions that link to each other,
		// along with a method for getting the most representative mention
		// Both sentence and token offsets start at 1!
		Map<Integer, CorefChain> graph = 
				document.get(CorefChainAnnotation.class);
	}
	
	public void createModels( Properties props)
	{
		pipeline = new StanfordCoreNLP(props);
		//Directory.SetCurrentDirectory(curDir);

	}

	public String getTokenDetails(Annotation document){
		
		// these are all the sentences in this document
		//a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
	    List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	    
	    String result = "";
	    for(CoreMap sentence: sentences) {
	    	result += "*******************************************SENTENCE BEGIN*****************************************************\n";
			
	        // traversing the words in the current sentence
	        // a CoreLabel is a CoreMap with additional token-specific methods
	        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	          String eachtokenDetails = "";
	          // this is the text of the token
	          String word = token.get(TextAnnotation.class);
	          // this is the POS tag of the token
	          String pos = token.get(PartOfSpeechAnnotation.class);
	          // this is the NER label of the token
	          String ne = token.get(NamedEntityTagAnnotation.class);     
	          eachtokenDetails = String.format("Token : %1$s \t POSTag: %2$s\t NERLabel: %3$s \n", word, pos, ne);
				result += eachtokenDetails;
	        }
	        result += "*******************************************SENTENCE END*****************************************************\n";

	        }
	  
	    System.out.println("TokenDetails: " + result.toString());
	    return result;
	}
}
