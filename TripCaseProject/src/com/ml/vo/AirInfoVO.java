package com.ml.vo;

import java.util.Calendar;
import java.util.Date;

public class AirInfoVO {

	private String emailID;
	private String suppplierCode;
	private String FlightNumber;
	private Integer SupplierPhoneNo;
	private Date dateOfDeparture;
	private String alternateFlightSchedule;
	/**
	 * @return the emailID
	 */
	public String getEmailID() {
		return emailID;
	}
	/**
	 * @param emailID the emailID to set
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	/**
	 * @return the suppplierCode
	 */
	public String getSuppplierCode() {
		return suppplierCode;
	}
	/**
	 * @param suppplierCode the suppplierCode to set
	 */
	public void setSuppplierCode(String suppplierCode) {
		this.suppplierCode = suppplierCode;
	}
	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return FlightNumber;
	}
	/**
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		FlightNumber = flightNumber;
	}
	/**
	 * @return the supplierPhoneNo
	 */
	public Integer getSupplierPhoneNo() {
		return SupplierPhoneNo;
	}
	/**
	 * @param supplierPhoneNo the supplierPhoneNo to set
	 */
	public void setSupplierPhoneNo(Integer supplierPhoneNo) {
		SupplierPhoneNo = supplierPhoneNo;
	}
	/**
	 * @return the dateOfDeparture
	 */
	public Date getDateOfDeparture() {
		return dateOfDeparture;
	}
	/**
	 * @param dateOfDeparture the dateOfDeparture to set
	 */
	public void setDateOfDeparture(Date dateOfDeparture) {
		this.dateOfDeparture = dateOfDeparture;
	}
	/**
	 * @return the alternateFlightSchedule
	 */
	public String getAlternateFlightSchedule() {
		return alternateFlightSchedule;
	}
	/**
	 * @param alternateFlightSchedule the alternateFlightSchedule to set
	 */
	public void setAlternateFlightSchedule(String alternateFlightSchedule) {
		this.alternateFlightSchedule = alternateFlightSchedule;
	}

}
