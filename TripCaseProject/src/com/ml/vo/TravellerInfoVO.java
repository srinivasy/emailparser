package com.ml.vo;

import java.io.Serializable;

public class TravellerInfoVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String emailID;
	private String tripTitle;
	private String location;
	private String supplierName;
	
	private Integer supplierPhoneNo;
	private String mapForLocation;
	private String wheatherForLocation;
	private String reservationStatus;
	private String billingAddress;
	AirInfoVO airEntityVO;
	CruiseInfoVO cruiseEntityVO;
	HotelInfoVO hotelEntityVO;
	ActivityInfoVO activityEntityVO;
	CarInfoVO carEntityVO;
	RailInfoVO railEntityVO;
	
	private String emailInput;
	
	/*public TravellerInfoVO(String emailInput){
		this.emailInput = emailInput;
	}*/
	
	/**
	 * @return the emailInput
	 */
	public String getEmailInput() {
		return emailInput;
	}
	/**
	 * @param emailInput the emailInput to set
	 */
	public void setEmailInput(String emailInput) {
		this.emailInput = emailInput;
	}
	/**
	 * @return the emailID
	 */
	public String getEmailID() {
		return emailID;
	}
	/**
	 * @param emailID the emailID to set
	 */
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	/**
	 * @return the tripTitle
	 */
	public String getTripTitle() {
		return tripTitle;
	}
	/**
	 * @param tripTitle the tripTitle to set
	 */
	public void setTripTitle(String tripTitle) {
		this.tripTitle = tripTitle;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}
	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	/**
	 * @return the supplierPhoneNo
	 */
	public Integer getSupplierPhoneNo() {
		return supplierPhoneNo;
	}
	/**
	 * @param supplierPhoneNo the supplierPhoneNo to set
	 */
	public void setSupplierPhoneNo(Integer supplierPhoneNo) {
		this.supplierPhoneNo = supplierPhoneNo;
	}
	/**
	 * @return the mapForLocation
	 */
	public String getMapForLocation() {
		return mapForLocation;
	}
	/**
	 * @param mapForLocation the mapForLocation to set
	 */
	public void setMapForLocation(String mapForLocation) {
		this.mapForLocation = mapForLocation;
	}
	/**
	 * @return the wheatherForLocation
	 */
	public String getWheatherForLocation() {
		return wheatherForLocation;
	}
	/**
	 * @param wheatherForLocation the wheatherForLocation to set
	 */
	public void setWheatherForLocation(String wheatherForLocation) {
		this.wheatherForLocation = wheatherForLocation;
	}
	/**
	 * @return the reservationStatus
	 */
	public String getReservationStatus() {
		return reservationStatus;
	}
	/**
	 * @param reservationStatus the reservationStatus to set
	 */
	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}
	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return billingAddress;
	}
	/**
	 * @param billingAddress the billingAddress to set
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	/**
	 * @return the airEntityVO
	 */
	public AirInfoVO getAirEntityVO() {
		return airEntityVO;
	}
	/**
	 * @param airEntityVO the airEntityVO to set
	 */
	public void setAirEntityVO(AirInfoVO airEntityVO) {
		this.airEntityVO = airEntityVO;
	}
	/**
	 * @return the cruiseEntityVO
	 */
	public CruiseInfoVO getCruiseEntityVO() {
		return cruiseEntityVO;
	}
	/**
	 * @param cruiseEntityVO the cruiseEntityVO to set
	 */
	public void setCruiseEntityVO(CruiseInfoVO cruiseEntityVO) {
		this.cruiseEntityVO = cruiseEntityVO;
	}
	/**
	 * @return the hotelEntityVO
	 */
	public HotelInfoVO getHotelEntityVO() {
		return hotelEntityVO;
	}
	/**
	 * @param hotelEntityVO the hotelEntityVO to set
	 */
	public void setHotelEntityVO(HotelInfoVO hotelEntityVO) {
		this.hotelEntityVO = hotelEntityVO;
	}
	/**
	 * @return the activityEntityVO
	 */
	public ActivityInfoVO getActivityEntityVO() {
		return activityEntityVO;
	}
	/**
	 * @param activityEntityVO the activityEntityVO to set
	 */
	public void setActivityEntityVO(ActivityInfoVO activityEntityVO) {
		this.activityEntityVO = activityEntityVO;
	}
	/**
	 * @return the carEntityVO
	 */
	public CarInfoVO getCarEntityVO() {
		return carEntityVO;
	}
	/**
	 * @param carEntityVO the carEntityVO to set
	 */
	public void setCarEntityVO(CarInfoVO carEntityVO) {
		this.carEntityVO = carEntityVO;
	}
	/**
	 * @return the railEntityVO
	 */
	public RailInfoVO getRailEntityVO() {
		return railEntityVO;
	}
	/**
	 * @param railEntityVO the railEntityVO to set
	 */
	public void setRailEntityVO(RailInfoVO railEntityVO) {
		this.railEntityVO = railEntityVO;
	}
	
	

}
